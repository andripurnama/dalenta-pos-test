import { PRODUCT_TYPES } from './types';

export const getProducts = () => ({
    type: PRODUCT_TYPES.LOAD_PRODUCTS
});

export const getProductsSuccess = (products: any) => ({
    type: PRODUCT_TYPES.LOAD_PRODUCTS,
    payload: products
});

export const getProductsFailure = () => ({
    type: PRODUCT_TYPES.LOAD_PRODUCTS_ERROR,
});

export function fetchProducts() {
    return async (dispatch: any) => {
      dispatch(getProducts())
  
      try {
        const { products } :any = await fetch('http://f-test.dalenta.tech/product')
        // const data = await response.json()
  
        dispatch(getProductsSuccess(products))
      } catch (error) {
        dispatch(getProductsFailure())
      }
    }
}