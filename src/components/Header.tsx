import * as React from 'react';
import { TrashIcon, CopyIcon } from '../assets';

export const Header = () => {
    return (
        <section className="d-flex justify-content-start" id="Header">
            <div className="mr-4">
                <select name="Category" id="">
                    <option value="Product">Products</option>
                    <option value="Variant">Variants</option>
                </select>
            </div>
            <div className="mr-4">
                <button><img src={ CopyIcon } className="button__primary button__shadow button__radius"/>Duplicate</button>
                <button><img src={ TrashIcon } className="button__primary button__shadow button__radius"/>Delete</button>
            </div>
        </section>
    );
}