import * as views from '../../pages';

export interface AppRoute {
    title: string;
    path?: string;
    component: any;
    exact?: boolean;
    key?: number;
}

export const routes: AppRoute[] = [
    {
        title: 'Dalenta Pos - Login',
        path: '/',
        component: views.Login,
        exact: true
    },
    {
        title: 'Dashboard',
        path: '/dashboard',
        component: views.Dashboard,
        exact: true
    },
    {
        title: 'Products',
        path: '/products',
        component: views.Product,
        exact: true
    },
    
];
