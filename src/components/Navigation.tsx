import React from 'react';
import { NavLink } from 'react-router-dom';
import { DashboardIcon, DevicesIcon, HomeIcon, ProductIcon, SettingsIcon, StatisticsIcon, UserIcon } from '../assets';

export const Navigation = () => {
    return (
        <aside>
            <div className="col-1">
                <nav className="nav flex-column">
                    <NavLink className="nav-link" to="/dashboard">
                        <img src={ DashboardIcon } alt=""/>
                    </NavLink>
                    <NavLink className="nav-link" to="/devices">
                        <img src={ DevicesIcon } alt=""/>
                    </NavLink>
                    <NavLink className="nav-link" to="/home">
                        <img src={ HomeIcon } alt=""/>
                    </NavLink>
                    <NavLink className="nav-link" to="/products">
                        <img src={ ProductIcon } alt=""/>
                    </NavLink>
                    <NavLink className="nav-link" to="/settings">
                        <img src={ SettingsIcon } alt=""/>
                    </NavLink>
                    <NavLink className="nav-link" to="/statistics">
                        <img src={ StatisticsIcon } alt=""/>
                    </NavLink>
                    <NavLink className="nav-link" to="/user">
                        <img src={ UserIcon } alt=""/>
                    </NavLink>
                </nav>
            </div>
        </aside>
    )
}