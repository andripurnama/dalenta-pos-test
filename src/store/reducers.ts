import { combineReducers } from 'redux';
import { product } from '../pages/Product/redux';
export default combineReducers({ product });