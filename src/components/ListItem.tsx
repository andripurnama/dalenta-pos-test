import * as React from 'react';

interface Item {
    image: string,
    ProductCode: string,
    ProductName: string,
    variantsName: string,
    categoryName: string
    productPrice: number,
    productCost: number
}
export const ListItem = ({ image, ProductCode, ProductName, variantsName, categoryName, productCost, productPrice } :Item) => {
    return (
        <div className="bg__grey text-white w-100">
            <div className="image mr-3">
                <img src={image} alt={ProductName} />
            </div>
            <div className="product__name">
                <span>{ ProductName }</span>
            </div>
            <div className="product__name">
                <span>{ ProductName }</span>
            </div>
        </div>
    )
}