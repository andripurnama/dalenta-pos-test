import React from 'react';

export const Login:React.FC<any> = () => {
    return (
        <section id="Login">
            <div className="container">
                <div className="row justify-content-center align-items-center">
                    <div className="col-md-6">
                        <div className="col-md-12">
                            <form action="#" className="form">
                                <h3 className="text-center text-info">Login</h3>
                                <div className="form-group">
                                    <label htmlFor="username" className="text-info">Username</label>
                                    <input type="text" name="username" id="username" className="form-control" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="password" className="text-info">Password</label>
                                    <input type="text" name="password" id="username" className="form-control" />
                                </div>
                                <button className="btn btn-primary btn-lg">Login</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>    
        </section>
    );
};