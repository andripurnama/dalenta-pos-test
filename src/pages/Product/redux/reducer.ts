import { PRODUCT_TYPES } from './types';

const INITIAL_STATE: any = {
    products: []
};

export default (state: any = INITIAL_STATE, action: any) => {
    switch (action.type) {
        case PRODUCT_TYPES.LOAD_PRODUCTS:
            return {
                ...state
            };
        case PRODUCT_TYPES.LOAD_PRODUCTS_SUCCESS:
            return {
                ...state,
                products:action.payload,
            };
        case PRODUCT_TYPES.LOAD_PRODUCTS_ERROR:
            return {
                ...state
            };
        default:
            return state || INITIAL_STATE;
    }
};
